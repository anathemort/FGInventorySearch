function onInit()
    local ruleset = User.getRulesetName();

    filterOptions = {
        [1] = {
            sLabelRes = "filteropt_none",
            fFilter = nil },
        [2] = {
            sLabelRes = "filteropt_armor",
            sOptKey = "ISopt_armor",
            fFilter = function(item)
                return (ItemManager2.isArmor(item));
            end },
        [3] = {
            sLabelRes = "filteropt_weapons",
            sOptKey = "ISopt_weapons",
            fFilter = function(item)
                return (ItemManager2.isWeapon(item));
            end },
        [4] = {
            sLabelRes = "filteropt_magical",
            sOptKey = "ISopt_magical",
            fFilter = function(item)
                if LibraryData.getIDState("item", item, true) == false then
                    return false; -- do not reveal that unidentified items are magical
                end

                if ruleset == "5E" then
                    local sProp = DB.getValue(item, "properties", ""):lower();
                    local sType = DB.getValue(item, "type", "");

                    return sProp:match("magic") or sType == "Wondrous Item" or sType == "Potion" or CharAttunementManager.doesItemAllowAttunement(item);
                elseif ruleset == "4E" then
                    local sClass = DB.getValue(item, "class", "");

                    return sClass == "Potion" or sClass == "Consumable" or sClass == "Wondrous Item" or sClass == "Implements" or sClass == "Arms Slot" or sClass == "Feet Slot" or sClass == "Hands Slot" or sClass == "Head Slot" or sClass == "Neck Slot" or sClass == "Ring Slot" or sClass == "Waist Slot";
                else
                    local sAura = DB.getValue(item, "aura", "");
                    local sType = DB.getValue(item, "type", "");

                    return (sAura ~= "" and string.match(sAura, "nonmagical") == nil) or sType == "Potion";
                end
            end },
        [5] = {
            sLabelRes = "filteropt_potions",
            sOptKey = "ISopt_potions",
            fFilter = function(item)
                if ruleset == "4E" then
                    return DB.getValue(item, "class", "") == "Potion" or DB.getValue(item, "subclass", "") == "Potion";
                end

                return DB.getValue(item, "type", "") == "Potion" or DB.getValue(item, "subtype", "") == "Potion";
            end },
        [6] = {
            sLabelRes = "filteropt_scrolls",
            sOptKey = "ISopt_scrolls",
            fFilter = function(item)
                if ruleset == "4E" then
                    return DB.getValue(item, "class", "") == "Scroll" or DB.getValue(item, "subclass", "") == "Scroll";
                end

                return DB.getValue(item, "type", "") == "Scroll" or DB.getValue(item, "subtype", "") == "Scroll";
            end },
        [7] = {
            sLabelRes = "filteropt_ritual",
            sOptKey = "ISopt_ritual",
            sRulesetFilter = "4E",
            fFilter = function(item)
                return DB.getValue(item, "class", "") == "Ritual";
            end },
        [8] = {
            sLabelRes = "filteropt_id",
            sOptKey = "ISopt_id",
            fFilter = function(item)
                return (LibraryData.getIDState("item", item, true)) == true;
            end },
        [9] = {
            sLabelRes = "filteropt_not_id",
            sOptKey = "ISopt_not_id",
            fFilter = function(item)
                return (LibraryData.getIDState("item", item, true)) == false;
            end },
        [10] = {
            sLabelRes = "filteropt_gear",
            sOptKey = "ISopt_gear",
            fFilter = function(item)
                if ruleset == "4E" then
                    return DB.getValue(item, "class", "") == "Gear" or DB.getValue(item, "subclass", "") == "Gear";
                end

                return DB.getValue(item, "type", "") == "Adventuring Gear" or DB.getValue(item, "subtype", "") == "Adventuring Gear";
            end },
        [11] = {
            sLabelRes = "filteropt_goods",
            sOptKey = "ISopt_goods",
            fFilter = function(item)
                local sType = DB.getValue(item, "type", "");
                local sSubType = DB.getValue(item, "subtype", "");

                return sType == "Goods and Services" or sSubType == "Goods and Services" or sType == "Tools" or sType:match("Mounts") or sType:match("Vehicles");
            end },
        [12] = {
            sLabelRes = "filteropt_carried",
            sOptKey = "ISopt_carried",
            fFilter = function(item)
                return DB.getValue(item, "carried", "") == 1;
            end },
        [13] = {
            sLabelRes = "filteropt_equipped",
            sOptKey = "ISopt_equipped",
            fFilter = function(item)
                return DB.getValue(item, "carried", "") == 2;
            end },
        [14] = {
            sLabelRes = "filteropt_not_carried",
            sOptKey = "ISopt_not_carried",
            fFilter = function(item)
                return DB.getValue(item, "carried", "") == 0;
            end },
    };

    for _, v in ipairs(filterOptions) do
        if v.sOptKey ~= nil and (v.sRulesetFilter == nil or v.sRulesetFilter == ruleset) then
            OptionsManager.registerOption2(v.sOptKey, true, "option_header_IF", v.sLabelRes, "option_entry_cycler",
                    { labels = "option_val_on", values = "on", baselabel = "option_val_off", baseval = "off", default = "on" });
            OptionsManager.registerCallback(v.sOptKey, onOptionChanged);
        end
    end
end

function findFilterOption(sLabelRes, sLabelValue, sOptKey)
    local option;

    for _, v in ipairs(InventoryFiltersManager.filterOptions) do
        if sLabelRes ~= nil and sLabelRes == v.sLabelRes then
            option = v;
        elseif sLabelValue ~= nil and Interface.getString(v.sLabelRes) == sLabelValue then
            option = v;
        elseif sOptKey ~= nil and sOptKey == v.sOptKey then
            option = v;
        end
    end

    return option;
end

local aFilterOptionListeners = {};
function setFilterOptionListener(f)
    table.insert(aFilterOptionListeners, f);
end
function removeFilterOptionListener(f)
    for k, v in ipairs(aFilterOptionListeners) do
        if v == f then
            table.remove(aFilterOptionListeners, k);
            return true;
        end
    end

    return false;
end

function onOptionChanged(sKey)
    for _, v in pairs(aFilterOptionListeners) do
        v(sKey);
    end
end
